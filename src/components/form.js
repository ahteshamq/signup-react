import React, { Component } from 'react';
import validator from 'validator';
import '../styles/form.css'


class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFirstNameValid: false,
            isLastNameValid: false,
            isAgeValid: false,
            isGenderValid: false,
            isRoleValid: false,
            isMailValid: false,
            isPassValid: false,
            isCnfPassValid: false,
            isCheckedTNC: false,
            firstNameClass: "form-control",
            lastNameClass: "form-control",
            ageClass: "form-select",
            emailClass: "form-control",
            genderClass: "form-select",
            roleClass: "form-select",
            passClass: "form-control",
            pass: "",
            passError:"Please provide strong password. Your password must contain a [A-Z], a [a-z], a [0-9] and a special character. Minimum password length should be 8 characters.",
            confirmClass: "form-control",
            checkClass: "form-check-input",
            successDisplay: false
        };
    }

    handleFirstName = (event) => {
        let name = event.target.value
        if (!validator.isAlpha(name)) {
            this.setState({
                isFirstNameValid: false,
                firstNameClass: "form-control is-invalid"
            })
        } else {
            this.setState({
                isFirstNameValid: true,
                firstNameClass: "form-control is-valid"
            })
        }

    }

    handleLastName = (event) => {
        let lastname = event.target.value
        if (!validator.isAlpha(lastname)) {
            this.setState({
                isLastNameValid: false,
                lastNameClass: "form-control is-invalid"
            })
        } else {
            this.setState({
                isLastNameValid: true,
                lastNameClass: "form-control is-valid"
            })
        }

    }

    handleAge = (event) => {
        let age = event.target.value
        !validator.isInt(age)? this.setState({
            isAgeValid: false,
            ageClass: "form-control is-invalid"
        }) : this.setState({
            isAgeValid: true,
            ageClass: "form-control is-valid"
        })
    }

    handleEmail = (event) => {
        let email = event.target.value
        !validator.isEmail(email)? this.setState({
            isMailValid: false,
            emailClass: "form-control is-invalid"
        }) : this.setState({
            isMailValid: true,
            emailClass: "form-control is-valid"
        })
    }

    handleGender = (event) => {
        let option = event.target.value
        option === '' ? this.setState({
            isGenderValid: false,
            genderClass: "form-select is-invalid"
        }) : this.setState({
            isGenderValid: true,
            genderClass: "form-select is-valid"
        })
    }

    handleRole = (event) => {
        let option = event.target.value
        option === '' ? this.setState({
            isRoleValid: false,
            roleClass: "form-select is-invalid"
        }) : this.setState({
            isRoleValid: true,
            roleClass: "form-select is-valid"
        })
    }

    handlePassword = (event) => {
        let password = event.target.value
        let passStr = this.state.passError
        if(!validator.isStrongPassword(password)){
            let errorStr = "Please provide strong password. Your password must contain "
            if(validator.isEmpty(password)){
                errorStr = passStr
            }else{
                if(/[A-Z]/.test(password) === false){
                    errorStr += "a [A-Z], "
                }
                if(/[a-z]/.test(password) === false){
                    errorStr += "a [a-z], "
                }
                if(/[0-9]/.test(password) === false){
                    errorStr += "a [0-9], "
                }
                if(/[-’/`~!#*$@_%+=.,^&(){}[\]|;:”<>?\\]/.test(password) === false){
                    errorStr += "and a speacial character."
                }
                if(password.length<8){
                    errorStr += "Minimum password length should be 8 characters."
                }
            }
            this.setState({
                isPassValid:false,
                passError: errorStr,
                passClass: "form-control is-invalid",
            })
        }else{
            this.setState({
                isPassValid: true,
                passClass: "form-control is-valid",
                pass: password,
                passError: passStr
            })
        }
    }

    handleConfirmPassword = (event) => {
        let pass = this.state.pass
        let cnfpass = event.target.value
        pass !== cnfpass ? this.setState({
            isCnfPassValid: false,
            confirmClass: "form-control is-invalid"
        }) : this.setState({
            isCnfPassValid: true,
            confirmClass: "form-control is-valid"
        })
    }

    handleCheck = (event) => {
        let check = event.target.checked
        console.log(typeof check)
        !check ? this.setState({
            isCheckedTNC: false,
            checkClass: "form-check-input is-invalid"
        }) : this.setState({
            isCheckedTNC: true,
            checkClass: "form-check-input is-valid"
        })
    }

    handleSubmit = (event) => {
        event.preventDefault()
        let isFormOK = 0

        this.state.isFirstNameValid ? isFormOK+=1 : this.setState({
            firstNameClass: "form-control is-invalid"
        }) 

        console.log(typeof isFormOK,isFormOK)
        this.state.isLastNameValid ? isFormOK+=1 : this.setState({
            lastNameClass: "form-control is-invalid"
        })

        this.state.isAgeValid ? isFormOK+=1 : this.setState({
            ageClass: "form-control is-invalid"
        })

        this.state.isMailValid ? isFormOK+=1 : this.setState({
            emailClass: "form-control is-invalid"
        })

        this.state.isPassValid ? isFormOK+=1 : this.setState({
            passClass: "form-control is-invalid"
        })

        this.state.isCnfPassValid ? isFormOK+=1 : this.setState({
            confirmClass: "form-control is-invalid"
        })

        this.state.isRoleValid ? isFormOK+=1 : this.setState({
            roleClass: "form-select is-invalid"
        })

        this.state.isGenderValid ? isFormOK+=1 : this.setState({
            genderClass: "form-select is-invalid"
        })

        this.state.isCheckedTNC ? isFormOK+=1 : this.setState({
            checkClass: "form-check-input is-invalid"
        })

        isFormOK === 9 && this.setState({
            successDisplay: true
        })
    }

    render() {
        return (
            <div className='container mt-4'>
                <h1 className='form-title'>Sign up</h1>
                <form>
                    <div className='row'>
                        <div className="col-md-6">
                            <label htmlFor="firstName" className="form-label">First name</label>
                            <div className='input-group'>
                                <span className="input-group-text"><i className="fa fa-user"></i></span>
                                <input
                                    type="text"
                                    className={this.state.firstNameClass}
                                    id="firstName"
                                    onChange={this.handleFirstName}
                                />
                                <div className="invalid-feedback">
                                    Please provide a valid name. Your name should not contain numbers or speacial characters
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="validationServer02" className="form-label">Last name</label>
                            <div className='input-group'>
                                <span className="input-group-text"><i className="fa fa-group"></i></span>
                                <input
                                    type="text"
                                    className={this.state.lastNameClass}
                                    onChange={this.handleLastName}
                                />
                                <div className="invalid-feedback">
                                    Please provide a valid name. Your name should not contain numbers or speacial characters
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className="col-md-6">
                            <label htmlFor="validationServerUsername" className="form-label">Age</label>
                            <div className="input-group has-validation">
                                <span className="input-group-text"><i className="fa-solid fa-user"></i></span>
                                <input
                                    type="text"
                                    className={this.state.ageClass}
                                    onChange={this.handleAge} />
                                <div className="invalid-feedback">
                                    Please provide a valid age. Age must be an integer.
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="validationServer04" className="form-label">Gender</label>
                            <div className='input-group'>
                                <span className="input-group-text"><i className="fas fa-mars"></i></span>
                                <select className={this.state.genderClass} id="validationServer04" 
                                onChange={this.handleGender}>
                                    <option defaultValue value="">Select gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Others">Others</option>
                                    <option value="CNTR">Choose not to response</option>
                                </select>
                                <div id="validationServer04Feedback" className="invalid-feedback">
                                    Please select an option.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <label htmlFor="validationServer05" className="form-label">Role</label>
                        <div className='input-group'>
                            <span className="input-group-text"><i className="fa fa-briefcase"></i></span>
                            <select className={this.state.roleClass} id="validationServer01"  
                            onChange={this.handleRole}>
                                <option defaultValue value="">Select role</option>
                                <option>Developer</option>
                                <option>Senior developer</option>
                                <option>Lead Engineer</option>
                                <option>CTO</option>
                            </select>
                            <div id="validationServer04Feedback" className="invalid-feedback">
                                Please select role.
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <label htmlFor="validationServer03" className="form-label">Email</label>
                        <div className='input-group'>
                            <span className="input-group-text"><i className="fa fa-envelope"></i></span>
                            <input type="text" className={this.state.emailClass} id="validationServer03"  
                            onChange={this.handleEmail} />
                            <div id="validationServer03Feedback" className="invalid-feedback">
                                Please provide a valid mail.
                            </div>
                        </div>
                    </div>

                    <div className="col-md-12">
                        <label htmlFor="validationServer05" className="form-label">Password</label>
                        <div className='input-group'>
                            <span className="input-group-text"><i className="fa fa-lock"></i></span>
                            <input
                                type="password"
                                className={this.state.passClass}
                                id="validationServer05"
                                onChange={this.handlePassword} />
                            <div id="validationServer05Feedback" className="invalid-feedback">
                                {this.state.passError}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <label htmlFor="validationServer06" className="form-label">Confirm Password</label>
                        <div className='input-group'>
                            <span className="input-group-text"><i className="fa fa-lock"></i></span>
                            <input
                                type="password"
                                className={this.state.confirmClass}
                                id="validationServer06"
                                onChange={this.handleConfirmPassword} />
                            <div id="validationServer06Feedback" className="invalid-feedback">
                                Password did not match.
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className='row'>
                            <div className="form-check col-md-6 mt-2 ml-3">
                                <input className={this.state.checkClass} type="checkbox" value="" id="invalidCheck3"  onChange={this.handleCheck} />
                                <label className="form-check-label ml-3">
                                    Agree to terms and conditions
                                </label>
                                <div id="invalidCheck3Feedback" className="invalid-feedback">
                                    You must agree before submitting.
                                </div>
                            </div>
                            <div className="col-md-6 mt-2 mb-3">
                                <button className="btn btn-primary" type="submit" onClick={this.handleSubmit}>Sign Up</button>
                            </div>
                        </div>
                    </div>
                </form>
                {this.state.successDisplay && <h1 className='form-success'>Signed Up Successfully!!!!</h1>}
            </div>
        );
    }
}

export default Form;